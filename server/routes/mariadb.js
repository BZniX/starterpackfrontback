import express from 'express';
import connect from '../helpers/mariadb';
const router = express.Router();

router.post('/text/mariadb', function(req,res) {
  const request = `insert into toto (data) values ('${req.body.data}')`;
  connect.query(request, (err, response) => {
    if (!err) {
      res.send({bool: true, res: response, flash: 'Worked Sql'})
    } else {
      res.send(err)
    }
  });
});

router.get('/text/mariadb/total', function(req,res) {
  const request = `select * from toto`;
  connect.query(request, (err, response) => {
    if (!err) {
      res.send(response)
    } else {
      res.send(err)
    }
  });
});

router.post('/text/mariadb/delete', function(req,res) {
  const request = `delete from toto`;
  connect.query(request, (err) => {
    if (!err) {
      res.send({flash: 'all deleted'})
    } else {
      res.send(err)
    }
  });
});

export default router;
