import mysql from 'mysql';

const connect = mysql.createConnection({
  host: '21507d35353e', // docker ps => container_id mariadb
  user: 'root',
  password: 'root',
  database: 'starterdocker'
});

connect.connect((err) => {
  if (!err) {
    console.log('MARIADB connected');
  } else {
    console.log('err : ', err);
    console.log('Error connecting database');
  }
});

export default connect;
