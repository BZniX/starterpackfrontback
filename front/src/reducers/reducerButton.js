const reducerButton = (state = false, action) => {
	switch (action.type) {
		case 'GET_HIDDEN':
			return action.payload;
		default:
			return state;
	}
};

export default reducerButton;
