const reducerDelete = (state = "", action) => {
  switch (action.type) {
    case 'API_CALL_DELETE_TOTAL_LIST_SUCCESS' :
      return action.payload;
    case 'API_CALL_DELETE_TOTAL_LIST_FAILURE':
      return action.payload;
    default:
      return state;
  }
};

export default reducerDelete;
