const reducerAxios = (state = "", action) => {
	switch (action.type) {
		case 'API_AXIOS_SQL_SUCCESS' :
			return action.payload;
		case 'API_AXIOS_FAILURE':
			return action.payload;
		default:
			return state;
	}
};

export default reducerAxios;
