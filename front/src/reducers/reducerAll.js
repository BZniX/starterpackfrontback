const reducerAll = (state = "pas de message", action) => {
	switch (action.type) {
		case 'GET_MESSAGE':
			return action.payload;
		default:
			return state;
	}
};

export default reducerAll;
