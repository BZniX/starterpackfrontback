import { combineReducers } from 'redux';
import reducerAll from './reducerAll';
import reducerButton from './reducerButton';
import reducerAxios from './reducerAxios';
import reducerTotalList from './reducerTotalList';
import reducerDelete from './reducerDelete';

const rootReducer = combineReducers({
	message:reducerAll,
	buttonBool: reducerButton,
	messageAxios:reducerAxios,
  totalList: reducerTotalList,
	deleted: reducerDelete,
});

export default rootReducer;
