export const GET_MESSAGE = 'GET_MESSAGE';

export const GET_HIDDEN = 'GET_HIDDEN';

export const API_AXIOS_SQL_REQUEST = 'API_AXIOS_SQL_REQUEST';
export const API_AXIOS_SQL_SUCCESS = 'API_AXIOS_SQL_SUCCESS';
export const API_AXIOS_SQL_FAILURE = 'API_AXIOS_SQL_FAILURE';

export const API_CALL_TOTAL_LIST_REQUEST = 'API_CALL_TOTAL_LIST_REQUEST';
export const API_CALL_TOTAL_LIST_SUCCESS = 'API_CALL_TOTAL_LIST_SUCCESS';
export const API_CALL_TOTAL_LIST_FAILURE = 'API_CALL_TOTAL_LIST_FAILURE';

export const API_CALL_DELETE_TOTAL_LIST_REQUEST = 'API_CALL_DELETE_TOTAL_LIST_REQUEST';
export const API_CALL_DELETE_TOTAL_LIST_SUCCESS = 'API_CALL_DELETE_TOTAL_LIST_SUCCESS';
export const API_CALL_DELETE_TOTAL_LIST_FAILURE = 'API_CALL_DELETE_TOTAL_LIST_FAILURE';

export const API_CALL_REQUEST = 'API_CALL_REQUEST';