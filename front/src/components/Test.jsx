// @flow

import React, {Component} from "react";

type
State = {
  message: string,
  messageApi: string,
};

type
Props = {
  message: string,
  buttonBool: boolean,
  messageAxios: string,
  getFirstSaga: Function,
  deleted: string,
  totalList: Array<Object>,
};

class Test extends Component<Props, State> {
  props: Props;
  state: State = {
    message: "",
    messageApi: ""
  };
  
  componentWillMount() {
    this.props.getFirstSaga();
  }
  
  componentWillReceiveProps(nextProps: Object) {
    if(nextProps.messageAxios !== "") {
      this.setState({messageApi:nextProps.messageAxios}, () => setTimeout(() => {
        this.setState({messageApi:""})
      }, 1500))
    }
    if(nextProps.deleted !== "") {
      this.setState({messageApi:nextProps.deleted}, () => setTimeout(() => {
        this.setState({messageApi:""})
      }, 1500))
    }
  }
  
  render() {
    return (
      <div>
        {
          (this.props.buttonBool)
            ?
            <p>{this.props.message}</p>
            :
            <p>{this.state.message}</p>
        }
        <div>
          {
            (this.props.totalList.length !== 0)
            ?
              this.props.totalList.map(x => {
                return <p>{x.id} - {x.data}</p>
              })
              :
              <p>nothing</p>
          }
        </div>
        <p>{this.state.messageApi}</p>
      </div>
    );
  }
}

export default Test;


