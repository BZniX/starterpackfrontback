// @flow

import React, {Component} from 'react';

import Test from '../containers/containerTest';
import Test2 from '../containers/containerTest2';

class App extends Component<*> {
  render() {
    return (
      <div>
        <Test2/>
        <Test/>
      </div>
    );
  }
}

export default App;
