// @flow

import React, {Component} from "react";

type
State = {
  button: boolean,
  message: string,
  valueAxios: string,
};

type
Props = {
  getHidden: Function,
  getMessage: Function,
  getAxiosSql: Function,
  getTotalList: Function,
  deleteTotalList: Function,
};

class Test2 extends Component<Props, State> {
  props: Props;
  state: State = {
    button: false,
    message: "",
    valueAxios: ""
  };
  
  handleChange = (event: Object) => {
    this.setState({message: event.target.value})
  };
  
  handleSubmit = () => {
    this.props.getMessage(this.state.message);
    this.setState({button: !this.state.button},
      () => {
        this.props.getHidden(this.state.button);
        this.props.getMessage(this.state.message);
      }
    );
    
  };
  
  handleClick = () => {
    this.setState({button: !this.state.button}, () => this.props.getHidden(this.state.button));
  };
  
  handleChangeAxios = (event: Object) => {
    this.setState({valueAxios:event.target.value})
  };
  
  handleAxiosSql = () => {
    this.props.getAxiosSql(this.state.valueAxios);
    this.props.getTotalList();
  };
  
  handleAxiosDelete = () => {
    this.props.deleteTotalList();
    this.props.getTotalList();
  };
  
  render() {
    return (
      <div>
        <input placeholder="message?" onChange={this.handleChange.bind(this)}/>
        <button onClick={this.handleSubmit.bind(this)}>PUSH</button>
        <button onClick={this.handleClick.bind(this)}>Toggle</button>
        <button onClick={this.handleAxiosSql}>AXIOS SQL</button>
        <input placeholder="message axios" onChange={(event) => this.handleChangeAxios(event)}/>
        <button onClick={this.handleAxiosDelete}>Delete list</button>
      </div>
    );
  }
}

export default Test2;


