import {
  GET_MESSAGE,
  GET_HIDDEN,
  API_AXIOS_SQL_REQUEST,
  API_CALL_REQUEST,
  API_CALL_TOTAL_LIST_REQUEST,
  API_CALL_DELETE_TOTAL_LIST_REQUEST,
} from '../constants/actions';

export function getMessage(message) {
	return { type: GET_MESSAGE, payload: message }
}

export function getHidden(bool) {
	return { type: GET_HIDDEN, payload: bool }
}

export function getAxiosSql(content) {
	return { type: API_AXIOS_SQL_REQUEST, payload: content }
}

export function getFirstSaga() {
  return { type: API_CALL_REQUEST }
}

export function getTotalList() {
  return { type: API_CALL_TOTAL_LIST_REQUEST }
}

export function deleteTotalList() {
  return { type: API_CALL_DELETE_TOTAL_LIST_REQUEST }
}