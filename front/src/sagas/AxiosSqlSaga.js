import { takeLatest, call, put } from 'redux-saga/effects';
import { API_END_POINT } from '../constants/api';
import axios from 'axios';

export default function* watcherSagaAxiosSql() {
  yield takeLatest("API_AXIOS_SQL_REQUEST", workerSagaAxiosSql);
}

function* workerSagaAxiosSql(content) {
  try {
    const response = yield call(fetchSagaAxiosSql, content.payload);
    const data = response.data;
    yield put({ type: "API_AXIOS_SQL_SUCCESS", payload: data.flash });
  } catch (error) {
    yield put({ type: "API_AXIOS_SQL_FAILURE", payload: error });
  }
}

function fetchSagaAxiosSql(obj) {
  return axios.post(`${API_END_POINT}/text/mariadb`, { data : obj });
}
