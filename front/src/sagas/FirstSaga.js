import { takeLatest, spawn } from 'redux-saga/effects';
import watcherSagaAxiosSql from './AxiosSqlSaga';
import watcherSagaAxiosTotalList from './AxiosTotalList';
import watcherSagaAxiosDelete from './AxiosDeleteTotalList';

export function* watcherSaga() {
  yield takeLatest("API_CALL_REQUEST", workerSaga);
}

function* workerSaga() {
  yield spawn (watcherSagaAxiosSql);
  yield spawn (watcherSagaAxiosTotalList);
  yield spawn (watcherSagaAxiosDelete);
}
