import { takeLatest, call, put } from 'redux-saga/effects';
import { API_END_POINT } from '../constants/api';
import axios from 'axios';

export default function* watcherSagaAxiosTotalList() {
  yield takeLatest("API_CALL_TOTAL_LIST_REQUEST", workerSagaAxiosTotalList);
}

function* workerSagaAxiosTotalList() {
  try {
    const response = yield call(fetchSagaAxiosTotalList);
    const data = response.data;
    yield put({ type: "API_CALL_TOTAL_LIST_SUCCESS", payload: data });
  } catch (error) {
    yield put({ type: "API_CALL_TOTAL_LIST_FAILURE", payload: error });
  }
}

function fetchSagaAxiosTotalList() {
  return axios.get(`${API_END_POINT}/text/mariadb/total`);
}
