import { takeLatest, call, put } from 'redux-saga/effects';
import { API_END_POINT } from '../constants/api';
import axios from 'axios';

export default function* watcherSagaAxiosDelete() {
  yield takeLatest("API_CALL_DELETE_TOTAL_LIST_REQUEST", workerSagaAxiosDelete);
}

function* workerSagaAxiosDelete() {
  try {
    const response = yield call(fetchSagaAxiosDelete);
    const data = response.data;
    yield put({ type: "API_CALL_DELETE_TOTAL_LIST_SUCCESS", payload: data.flash });
  } catch (error) {
    yield put({ type: "API_CALL_DELETE_TOTAL_LIST_FAILURE", payload: error });
  }
}

function fetchSagaAxiosDelete() {
  return axios.post(`${API_END_POINT}/text/mariadb/delete`);
}
