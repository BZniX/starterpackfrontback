import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Test2 from '../components/Test2'
import {getAxiosSql, getHidden, getMessage, deleteTotalList, getTotalList} from "../actions";

export default connect(null, dispatch => ({
    getMessage: bindActionCreators(getMessage, dispatch),
    getHidden: bindActionCreators(getHidden, dispatch),
    getAxiosSql: bindActionCreators(getAxiosSql, dispatch),
    deleteTotalList: bindActionCreators(deleteTotalList, dispatch),
    getTotalList: bindActionCreators(getTotalList, dispatch),
}))(Test2);

