import { connect } from 'react-redux';
import Test from '../components/Test';
import {bindActionCreators} from "redux";
import {getFirstSaga} from "../actions";

export default connect(state => ({
  message:state.message,
  buttonBool: state.buttonBool,
  messageAxios: state.messageAxios,
  totalList: state.totalList,
  deleted: state.deleted,
}), dispatch => ({
  getFirstSaga: bindActionCreators(getFirstSaga, dispatch),
}))(Test);

